package com.homesite.roofscore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoofscoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoofscoreApplication.class, args);
    }

}
