package com.homesite.roofscore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "hs_roof_material")
public class RoofMaterial {

    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "roof_key")
    private int roofKey;

    @Column(name = "roof_type")
    private String roofType;

    public RoofMaterial(UUID id, int roofKey, String roofType) {
        this.id = id;
        this.roofKey = roofKey;
        this.roofType = roofType;
    }

    public RoofMaterial(int roofKey, String roofType) {
        this.roofKey = roofKey;
        this.roofType = roofType;
    }

    public RoofMaterial() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getRoofKey() {
        return roofKey;
    }

    public void setRoofKey(int roofKey) {
        this.roofKey = roofKey;
    }

    public String getRoofType() {
        return roofType;
    }

    public void setRoofType(String roofType) {
        this.roofType = roofType;
    }

    @Override
    public String toString() {
        return "RoofMaterial{" +
                "id=" + id +
                ", roofKey=" + roofKey +
                ", roofType='" + roofType + '\'' +
                '}';
    }
}
