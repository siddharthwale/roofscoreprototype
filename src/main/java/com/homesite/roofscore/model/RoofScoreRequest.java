package com.homesite.roofscore.model;

import java.util.Date;

public class RoofScoreRequest {

    private int sessionId;
    private int quoteNumber;
    private String stateCode;
    private String county;
    private String roofMaterial;
    private int yearBuilt;
    private Date ratingDate;
    private Date policyEffectiveDate;

    public RoofScoreRequest(int sessionId, int quoteNumber, String stateCode, String county, String roofMaterial, int yearBuilt, Date ratingDate, Date policyEffectiveDate) {
        this.sessionId = sessionId;
        this.quoteNumber = quoteNumber;
        this.stateCode = stateCode;
        this.county = county;
        this.roofMaterial = roofMaterial;
        this.yearBuilt = yearBuilt;
        this.ratingDate = ratingDate;
        this.policyEffectiveDate = policyEffectiveDate;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getQuoteNumber() {
        return quoteNumber;
    }

    public void setQuoteNumber(int quoteNumber) {
        this.quoteNumber = quoteNumber;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getRoofMaterial() {
        return roofMaterial;
    }

    public void setRoofMaterial(String roofMaterial) {
        this.roofMaterial = roofMaterial;
    }

    public int getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(int yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public Date getRatingDate() {
        return ratingDate;
    }

    public void setRatingDate(Date ratingDate) {
        this.ratingDate = ratingDate;
    }

    public Date getPolicyEffectiveDate() {
        return policyEffectiveDate;
    }

    public void setPolicyEffectiveDate(Date policyEffectiveDate) {
        this.policyEffectiveDate = policyEffectiveDate;
    }

    @Override
    public String toString() {
        return "RoofScoreRequest{" +
                "sessionId=" + sessionId +
                ", quoteNumber=" + quoteNumber +
                ", stateCode='" + stateCode + '\'' +
                ", county='" + county + '\'' +
                ", roofMaterial='" + roofMaterial + '\'' +
                ", yearBuilt=" + yearBuilt +
                ", ratingDate=" + ratingDate +
                ", policyEffectiveDate=" + policyEffectiveDate +
                '}';
    }
}
