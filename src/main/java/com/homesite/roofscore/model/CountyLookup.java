package com.homesite.roofscore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "hs_county_lookup")
public class CountyLookup {

    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "state_name")
    private String stateName;

    @Column(name = "state_code")
    private String stateCode;

    @Column(name = "county_name")
    private String countyName;

    @Column(name = "county")
    private String county;

    @Column(name = "climate_zone")
    private String climateZone;

    public CountyLookup() {
    }

    public CountyLookup(UUID id, String stateName, String stateCode, String countyName, String county, String climateZone) {
        this.id = id;
        this.stateName = stateName;
        this.stateCode = stateCode;
        this.countyName = countyName;
        this.county = county;
        this.climateZone = climateZone;
    }

    public CountyLookup(String stateName, String stateCode, String countyName, String county, String climateZone) {
        this.stateName = stateName;
        this.stateCode = stateCode;
        this.countyName = countyName;
        this.county = county;
        this.climateZone = climateZone;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getClimateZone() {
        return climateZone;
    }

    public void setClimateZone(String climateZone) {
        this.climateZone = climateZone;
    }

    @Override
    public String toString() {
        return "CountyLookup{" +
                "id=" + id +
                ", stateName='" + stateName + '\'' +
                ", stateCode='" + stateCode + '\'' +
                ", countyName='" + countyName + '\'' +
                ", county='" + county + '\'' +
                ", climateZone='" + climateZone + '\'' +
                '}';
    }
}
