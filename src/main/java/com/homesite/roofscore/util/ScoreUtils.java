package com.homesite.roofscore.util;

public class ScoreUtils {

    public static double scoreThreeParameterWeibull(double ageYears, double alpha, double beta, double gamma) {
        double score = 0.0d;

        if (ageYears < 0.01d) {
            score = 1.0d;
        } else {
            if (ageYears <= gamma) {
                score = 1.0d;
            } else {
                double delta = ageYears - gamma;
                score = Math.exp(-1.0d * Math.pow(delta / beta, alpha));
            }
        }

        if (score <= Math.ulp(1.0f))
            score = 0.0d;

        return score;
    }
}
