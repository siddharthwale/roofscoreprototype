package com.homesite.roofscore.dto;

import java.util.ArrayList;

public class ParametersDto {
    private String hsRoofKey;
    private String climateZone;
    private String climateType;
    private String haagKeys;
    private ArrayList<String> haagLifeRange;
    private StatisticsDto sampleStatistics;
    private LearnedDto models;

    public ParametersDto(String hsRoofKey, String climateZone, String climateType, String haagKeys, ArrayList<String> haagLifeRange, StatisticsDto sampleStatistics, LearnedDto models) {
        this.hsRoofKey = hsRoofKey;
        this.climateZone = climateZone;
        this.climateType = climateType;
        this.haagKeys = haagKeys;
        this.haagLifeRange = haagLifeRange;
        this.sampleStatistics = sampleStatistics;
        this.models = models;
    }

    public String getHsRoofKey() {
        return hsRoofKey;
    }

    public String getClimateZone() {
        return climateZone;
    }

    public String getClimateType() {
        return climateType;
    }

    public String getHaagKeys() {
        return haagKeys;
    }

    public ArrayList<String> getHaagLifeRange() {
        return haagLifeRange;
    }

    public StatisticsDto getSampleStatistics() {
        return sampleStatistics;
    }

    public LearnedDto getModels() {
        return models;
    }

    @Override
    public String toString() {
        return hsRoofKey + ":" + climateZone;
    }  // Return the lookupKey
}
