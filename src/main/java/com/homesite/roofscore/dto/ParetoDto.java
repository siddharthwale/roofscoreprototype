package com.homesite.roofscore.dto;

public class ParetoDto {
    private double xi;
    private double sigma;

    public double getXi() {
        return xi;
    }

    public double getSigma() {
        return sigma;
    }
}
