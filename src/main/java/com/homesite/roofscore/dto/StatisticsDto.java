package com.homesite.roofscore.dto;

public class StatisticsDto {
    private double count;
    private double mean;
    private double median;
    private double minimum;
    private double maximum;
    private double standardDeviation;
    private double standardErrorOfMean;

    public double getCount() {
        return count;
    }

    public double getMean() {
        return mean;
    }

    public double getMedian() {
        return median;
    }

    public double getMinimum() {
        return minimum;
    }

    public double getMaximum() {
        return maximum;
    }

    public double getStandardDeviation() {
        return standardDeviation;
    }

    public double getStandardErrorOfMean() {
        return standardErrorOfMean;
    }
}
