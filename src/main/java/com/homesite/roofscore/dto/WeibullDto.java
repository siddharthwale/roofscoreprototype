package com.homesite.roofscore.dto;

import java.util.ArrayList;

public class WeibullDto {
    private double alpha;
    private ArrayList<Double> beta;

    public double getAlpha() {
        return alpha;
    }

    public ArrayList<Double> getBeta() {
        return beta;
    }
}
