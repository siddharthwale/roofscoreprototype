package com.homesite.roofscore.dto;

public class LearnedDto {
    private WeibullDto weibull;
    private ParetoDto paretoGeneralized;

    public WeibullDto getWeibull() {
        return weibull;
    }

    public ParetoDto getParetoGeneralized() {
        return paretoGeneralized;
    }
}
