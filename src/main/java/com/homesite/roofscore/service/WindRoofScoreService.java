package com.homesite.roofscore.service;


import com.homesite.roofscore.dto.ParametersDto;
import org.springframework.stereotype.Service;

@Service
public interface WindRoofScoreService {
    public double compute(double ageYears, ParametersDto p);
}
