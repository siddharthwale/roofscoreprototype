package com.homesite.roofscore.service;

import com.homesite.roofscore.model.RoofScoreRequest;
import org.springframework.stereotype.Service;

@Service
public interface RoofScoreService {
    double callWindAndWaterRoofScoreService(RoofScoreRequest roofScoreRequest);
}
