package com.homesite.roofscore.service.Impl;

import com.homesite.roofscore.dto.ParametersDto;
import com.homesite.roofscore.model.CountyLookup;
import com.homesite.roofscore.model.RoofMaterial;
import com.homesite.roofscore.model.RoofScoreRequest;
import com.homesite.roofscore.repository.CountyLookupRepository;
import com.homesite.roofscore.repository.ParameterRepository;
import com.homesite.roofscore.repository.RoofMaterialRepository;
import com.homesite.roofscore.service.RoofScoreService;
import com.homesite.roofscore.service.WindRoofScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneId;

@Service
public class RoofScoreServiceImpl implements RoofScoreService {

    private ParameterRepository parameterRepository;
    private RoofMaterialRepository roofMaterialRepository;
    private CountyLookupRepository countyLookupRepository;
    private WindRoofScoreService windRoofScoreService;

    @Autowired
    public RoofScoreServiceImpl(ParameterRepository parameterRepository,RoofMaterialRepository roofMaterialRepository,
                                CountyLookupRepository countyLookupRepository, WindRoofScoreService windRoofScoreService) {
        this.parameterRepository = parameterRepository;
        this.roofMaterialRepository = roofMaterialRepository;
        this.countyLookupRepository = countyLookupRepository;
        this.windRoofScoreService = windRoofScoreService;
    }

    @Override
    public double callWindAndWaterRoofScoreService(RoofScoreRequest roofScoreRequest) {
        ParametersDto parametersDto = null;
        int roofAge = roofScoreRequest.getRatingDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear() - roofScoreRequest.getYearBuilt();
        RoofMaterial roofMaterial = roofMaterialRepository.findByRoofType(roofScoreRequest.getRoofMaterial().toLowerCase());
        CountyLookup countyLookup = countyLookupRepository.findByStateCodeAndAndCounty(roofScoreRequest.getStateCode(), roofScoreRequest.getCounty());
        if(roofMaterial !=null && countyLookup!=null){
            String lookUpKey = roofMaterial.getRoofKey() + ":" + countyLookup.getClimateZone();
            parametersDto = parameterRepository.getParameter(lookUpKey);
        }
        return windRoofScoreService.compute(roofAge, parametersDto);
    }
}
