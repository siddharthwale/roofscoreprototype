package com.homesite.roofscore.service.Impl;

import com.homesite.roofscore.dto.LearnedDto;
import com.homesite.roofscore.dto.ParametersDto;
import com.homesite.roofscore.dto.StatisticsDto;
import com.homesite.roofscore.dto.WeibullDto;
import com.homesite.roofscore.service.WindRoofScoreService;
import com.homesite.roofscore.util.ScoreUtils;
import org.springframework.stereotype.Service;

@Service
public class WindRoofScoreServiceImpl implements WindRoofScoreService {
    @Override
    public double compute(double ageYears, ParametersDto p) {
        double minLife;
        double alpha, beta;

        try {
            StatisticsDto statistics = p.getSampleStatistics();
            minLife = statistics.getMinimum();
        } catch (Exception e) {
            minLife = 0.0d;
        }

        LearnedDto learned = p.getModels();
        WeibullDto weibull = learned.getWeibull();
        alpha = weibull.getAlpha();
        beta = weibull.getBeta().get(0);

        return ScoreUtils.scoreThreeParameterWeibull(ageYears, alpha, beta, minLife);
    }
}
