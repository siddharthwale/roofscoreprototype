package com.homesite.roofscore.controller;

import com.homesite.roofscore.model.RoofScoreRequest;
import com.homesite.roofscore.repository.ParameterRepository;
import com.homesite.roofscore.service.RoofScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoofScoreController {

    private ParameterRepository parameterRepository;
    private RoofScoreService roofScoreService;

    @Autowired
    public RoofScoreController(ParameterRepository parameterRepository, RoofScoreService roofScoreService) {
        this.parameterRepository = parameterRepository;
        this.roofScoreService = roofScoreService;
    }

    @RequestMapping(path = "/runWindAndWaterRoofScore", method = RequestMethod.POST)
    public double runWindAndWaterRoofScore(@RequestBody RoofScoreRequest roofScoreRequest) {
        return roofScoreService.callWindAndWaterRoofScoreService(roofScoreRequest);
    }
}
