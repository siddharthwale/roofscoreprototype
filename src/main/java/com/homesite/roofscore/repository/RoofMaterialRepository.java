package com.homesite.roofscore.repository;

import com.homesite.roofscore.model.RoofMaterial;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoofMaterialRepository extends CrudRepository<RoofMaterial, UUID> {
    RoofMaterial findByRoofType(String roofType);
}
