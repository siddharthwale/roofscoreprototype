package com.homesite.roofscore.repository;

import com.homesite.roofscore.model.CountyLookup;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface CountyLookupRepository extends CrudRepository<CountyLookup, UUID> {
    CountyLookup findByStateCodeAndAndCounty(String stateCode, String County);
}
