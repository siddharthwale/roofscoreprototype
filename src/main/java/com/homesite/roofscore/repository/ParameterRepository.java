package com.homesite.roofscore.repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.homesite.roofscore.dto.ParametersDto;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileReader;

@Component
public class ParameterRepository {

    @Value("${partmetersJsonFilePath}")
    private String parametersJsonPath;

    public ParametersDto getParameter(String lookupKey) {
        ParametersDto parameters = null;
        try {
            JSONObject json = (JSONObject) readJsonSimpleDemo(parametersJsonPath);
            Gson gson = new GsonBuilder().create();
            parameters = gson.fromJson(json.get(lookupKey).toString(), ParametersDto.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parameters;
    }

    public static Object readJsonSimpleDemo(String filename) throws Exception {
        FileReader reader = new FileReader(filename);
        JSONParser jsonParser = new JSONParser();
        return jsonParser.parse(reader);
    }
}
